#container persistent disk initializing

   parted /dev/sdb mklabel msdos
   parted /dev/sdb mkpart primary 512 100%
   mkfs.xfs /dev/sdb1 -f
# docker xfs persistent share
   mkdir /var/lib/docker
   echo `blkid /dev/sdb1 | awk '{print$2}' | sed -e 's/"//g'` /var/lib/docker   xfs   noatime,nobarrier   0   0 >> /etc/fstab
   mount /var/lib/docker

#bootstrap
   yum -y update
   yum -y upgrade
   yum install -y yum-utils device-mapper-persistent-data lvm2 mc
   yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
   yum -y update
   yum install -y docker-ce
# install docker-compose

   yum install -y epel-release
   yum install -y python-pip
   yum -y upgrade python*

   pip install --upgrade pip
   pip install docker-compose

   yum -y install jq htop dos2unix nmap

#ssh login enable:
   sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
   service sshd restart

# rc.local  after provisioning if reboot vm it will restart all of container.

   dos2unix /vagrant/rc.local
   cp /vagrant/rc.local /etc/rc.d/rc.local
   chmod 777 /etc/rc.d/rc.local


   chmod 777 /vagrant/bootstrap.sh
   dos2unix /vagrant/bootstrap.sh
   chmod 777 /vagrant/bootstrap-min.sh
   dos2unix /vagrant/bootstrap-min.sh
   cp /vagrant/bootstrap.sh /etc/rc.d/bootstrap.sh
   cp /vagrant/bootstrap-min.sh /etc/rc.d/bootstrap-min.sh


# start rc.local first time.
   sh /etc/rc.local
